/**
 * @author TatumCreative (Greg Tatum) / http://gregtatum.com/
 */

var twoPi = Math.PI * 2;

function updateGroupGeometry( mesh, geometry ) {

	if ( geometry.isGeometry ) {

		geometry = new THREE.BufferGeometry().fromGeometry( geometry );

		console.warn( 'THREE.GeometryBrowser: Converted Geometry to BufferGeometry.' );

	}

	mesh.children[ 0 ].geometry.dispose();
	mesh.children[ 1 ].geometry.dispose();

	mesh.children[ 0 ].geometry = new THREE.WireframeGeometry( geometry );
	mesh.children[ 1 ].geometry = geometry;

	// these do not update nicely together if shared

}

function CustomSinCurve( scale ) {

	THREE.Curve.call( this );

	this.scale = ( scale === undefined ) ? 1 : scale;

}

CustomSinCurve.prototype = Object.create( THREE.Curve.prototype );
CustomSinCurve.prototype.constructor = CustomSinCurve;

CustomSinCurve.prototype.getPoint = function ( t ) {

	var tx = t * 3 - 1.5;
	var ty = Math.sin( 2 * Math.PI * t );
	var tz = 0;

	return new THREE.Vector3( tx, ty, tz ).multiplyScalar( this.scale );

};

// heart shape

var x = 0, y = 0;

var heartShape = new THREE.Shape();

heartShape.moveTo( x + 5, y + 5 );
heartShape.bezierCurveTo( x + 5, y + 5, x + 4, y, x, y );
heartShape.bezierCurveTo( x - 6, y, x - 6, y + 7, x - 6, y + 7 );
heartShape.bezierCurveTo( x - 6, y + 11, x - 3, y + 15.4, x + 5, y + 19 );
heartShape.bezierCurveTo( x + 12, y + 15.4, x + 16, y + 11, x + 16, y + 7 );
heartShape.bezierCurveTo( x + 16, y + 7, x + 16, y, x + 10, y );
heartShape.bezierCurveTo( x + 7, y, x + 5, y + 5, x + 5, y + 5 );

var guis = {

	ConeGeometry: function ( mesh ) {

		var data = {
			radius: 7,
			height: 15,
			radialSegments: 4,
			heightSegments: 1,
			openEnded: false,
			thetaStart: 0,
			thetaLength: twoPi
		};

		function generateGeometry() {

			updateGroupGeometry( mesh,
				new THREE.ConeGeometry(
					data.radius,
					data.height,
					data.radialSegments,
					data.heightSegments,
					data.openEnded,
					data.thetaStart,
					data.thetaLength
				)
			);

		}
		generateGeometry();

	}

};

function chooseFromHash( mesh ) {

	var selectedGeometry = window.location.hash.substring( 1 ) || "ConeGeometry";

	if ( guis[ selectedGeometry ] !== undefined ) {

		guis[ selectedGeometry ]( mesh );

	}

	if ( selectedGeometry === 'TextGeometry' || selectedGeometry === 'TextBufferGeometry' ) {

		return { fixed: true };

	}

	//No configuration options
	return {};

}
